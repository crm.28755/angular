﻿using Microsoft.EntityFrameworkCore.Storage;

namespace Angular.DAL.Interfaces
{
    public interface IRepositoryProvider
    {
        IUserRepository Users { get; }
        IRegistrationRequetsRepository RegistrationRequets { get; }

        Task SaveChangesAsync();
        Task<IDbContextTransaction> BeginTransactionAsync();
    }
}