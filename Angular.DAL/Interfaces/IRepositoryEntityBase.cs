﻿namespace Angular.DAL.Interfaces
{
    public interface IRepositoryEntityBase<TEntity> where TEntity : class, IIdentifiable
    {
        void Update(TEntity item);
        Task AddAsync(TEntity item);
        Task<TEntity> GetByIdAsync(Guid id);
        Task<TEntity?> FindByIdAsync(Guid id);
        IQueryable<TEntity> SelectById(Guid id);
        IQueryable<TEntity> Select();
        void Remove(TEntity entity);
    }
}