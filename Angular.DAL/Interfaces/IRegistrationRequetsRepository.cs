﻿using Angular.DAL.Entities;

namespace Angular.DAL.Interfaces
{
    public interface IRegistrationRequetsRepository : IRepositoryEntityBase<RegistrationRequest>
    {
        Task<RegistrationRequest?> FindActualAsync(Guid id);
        IQueryable<RegistrationRequest> SelectExpired();
    }
}