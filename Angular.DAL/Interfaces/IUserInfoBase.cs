﻿namespace Angular.DAL.Interfaces
{
    internal interface IUserInfoBase : IIdentifiable
    {
        DateTime DateCreated { get; set; }
        string FirstName { get; set; }
        string Surname { get; set; }
        string Patronymic { get; set; }
        string Login { get; set; }
        byte[] PasswordHash { get; set; }
        byte[] PasswordSalt { get; set; }
    }
}