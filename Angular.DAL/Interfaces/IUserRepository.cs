﻿using Angular.DAL.Entities;

namespace Angular.DAL.Interfaces
{
    public interface IUserRepository : IRepositoryEntityBase<User>
    {
    }
}