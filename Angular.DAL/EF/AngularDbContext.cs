﻿using Angular.DAL.Entities;
using Microsoft.EntityFrameworkCore;

namespace Angular.DAL.EF
{
    public class AngularDbContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<RegistrationRequest> RegistionRequests { get; set; }

        public AngularDbContext(DbContextOptions<AngularDbContext> options) : base(options)
        {
            //Database.EnsureDeleted();
            //Database.EnsureCreated();
        }
    }
}