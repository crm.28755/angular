﻿using Angular.DAL.EF;
using Angular.DAL.Entities;
using Angular.DAL.Interfaces;
using Angular.DAL.Repositories;
using Microsoft.EntityFrameworkCore.Storage;
using System.Collections;

namespace Angular.DAL
{
    public class RepositoryProvider : IRepositoryProvider
    {
        public AngularDbContext Context { get; }
        private readonly Hashtable _h = new Hashtable();

        public IUserRepository Users => GetRep<UserRepository, User>();
        public IRegistrationRequetsRepository RegistrationRequets => GetRep<RegistrationRequetsRepository, RegistrationRequest>();

        public RepositoryProvider(AngularDbContext context)
        {
            Context = context;
        }

        public IRepositoryEntityBase<TEntity> GetRep<TEntity>() where TEntity : class, IIdentifiable
        {
            return GetRep<RepositoryEntityBase<TEntity>, TEntity>();
        }

        public T GetRep<T, TEntity>() where T : class, IRepositoryEntityBase<TEntity> where TEntity : class, IIdentifiable
        {
            var typeEntityBase = typeof(T);

            var key = typeEntityBase.FullName! + typeof(TEntity).FullName!;

            if (!_h.ContainsKey(key))
            {
                var rep = Activator.CreateInstance(typeEntityBase, this);

                _h[key] = rep;
            }

            return (T)_h[key]!;
        }

        public async Task SaveChangesAsync()
        {
            await Context.SaveChangesAsync();
        }
        
        public async Task<IDbContextTransaction> BeginTransactionAsync()
        {
            return await Context.Database.BeginTransactionAsync();
        }
    }
}