﻿using Angular.DAL.Entities;
using Angular.DAL.Interfaces;

namespace Angular.DAL.Repositories
{
    public class UserRepository : RepositoryEntityBase<User>, IUserRepository
    {
        public UserRepository(RepositoryProvider repProvider) : base(repProvider) { }
    }
}