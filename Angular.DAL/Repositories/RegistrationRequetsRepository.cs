﻿using Angular.DAL.Entities;
using Angular.DAL.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Angular.DAL.Repositories
{
    public class RegistrationRequetsRepository : RepositoryEntityBase<RegistrationRequest>, IRegistrationRequetsRepository
    {
        public RegistrationRequetsRepository(RepositoryProvider repProvider) : base(repProvider) { }

        public async Task<RegistrationRequest?> FindActualAsync(Guid id)
        {
            return await SelectById(id).Where(i => i.DateExpiration > DateTime.UtcNow).FirstOrDefaultAsync();
        }

        public IQueryable<RegistrationRequest> SelectExpired() 
        {
            return _dbSet.Where(i => i.DateExpiration <= DateTime.UtcNow);
        }
    }
}