﻿using Angular.DAL.Interfaces;

namespace Angular.DAL.Entities
{
    public class RegistrationRequest : IUserInfoBase
    {
        public Guid Id { get; set; }
        public DateTime DateCreated { get; set; }
        public string FirstName { get; set; } = null!;
        public string Surname { get; set; } = null!;
        public string Patronymic { get; set; } = null!;
        public byte[] PasswordHash { get; set; } = null!;
        public byte[] PasswordSalt { get; set; } = null!;
        public string Login { get; set; } = null!;
        public string Email { get; set; } = null!;
        public DateTime DateExpiration { get; set; }
    }
}