﻿using Angular.BLL.Helpers;
using Angular.DAL.Interfaces;
using Angular.Extensions;
using Angular.Helpers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Angular.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class UserController : ControllerBase
    {
        private readonly IRepositoryProvider _repProvider;

        public UserController(IRepositoryProvider repProvider)
        {
            _repProvider = repProvider;
        }

        [HttpGet("get-all")]
        public async Task<IActionResult> GetAll([FromQuery] UserParams userParams)
        {
            var q = _repProvider.Users
                .Select()
                .Select(i => new
                {
                    i.Id,
                    i.FirstName,
                    i.Surname,
                    i.Patronymic,
                    i.Email,
                    i.Login,
                });

            var res = await PagedList<object>.CreateAsync(q, userParams.PageNumber, userParams.PageSize); ;

            Response.AddPaginationHeader(new PaginationHeader(res.CurrentPage, res.PageSize, res.TotalCount, res.TotalPages));

            return Ok(res);
        }

        [HttpDelete("remove")]
        public async Task<IActionResult> Remove(Guid userId)
        {
            await _repProvider.Users
                .SelectById(userId)
                .ExecuteDeleteAsync();

            return Ok();
        }
    }
}