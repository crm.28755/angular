﻿using Angular.BLL.DTOs;
using Angular.BLL.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace Angular.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class AccountController : ControllerBase
    {
        private readonly IUserService _userService;
        private readonly ITokenService _tokenService;

        public AccountController(IUserService userService, ITokenService tokenService)
        {
            _userService = userService;
            _tokenService = tokenService;
        }

        [HttpPost("register")]
        public async Task<IActionResult> Register([FromBody] RegisterDto model)
        {
            await _userService.RegisterAsync(model);

            return Ok();
        }

        [HttpGet("confirm")]
        public async Task<IActionResult> Confirm(Guid id)
        {
            await _userService.ConfirmAsync(id);

            return Ok();
        }

        [HttpPost("login")]
        public async Task<IActionResult> Login([FromBody] LoginDto model)
        {
            var user = await _userService.LoginAsync(model);

            var token = _tokenService.CreateToken(user);

            var res = new
            {
                UserName = user.Login,
                Token = token
            };

            return Ok(res);
        }
    }
}