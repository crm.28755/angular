import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { BehaviorSubject, map } from "rxjs";
import { User } from "../_models/user";

@Injectable({ providedIn: 'root' })

export class AccountService {
  private basePath = "/api/account/";
  constructor(private http: HttpClient) { }

  public user: User | undefined;
  private currentUserSource = new BehaviorSubject<User | null>(null)
  currentUser$ = this.currentUserSource.asObservable();
  register(model: any) {
    return this.http.post(this.basePath + "register", model);
  }

  login(model: any) {
    return this.http.post<User>(this.basePath + "login", model).pipe(map((user: User) => {
      if (user) {
        localStorage.setItem("user", JSON.stringify(user));
        this.setCurrentUser(user);
      }
      return user;
    }));
  }

  setCurrentUser(user: User | null) {
    this.currentUserSource.next(user);
  }

  logout() {
    localStorage.removeItem("user");
    this.setCurrentUser(null);
  }

  confirm(id: string) {
    return this.http.get<any>(this.basePath + "confirm",
      {
        params: { id: id }
      });
  }
}
