import { HttpClient, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { map } from "rxjs";
import { PaginationResult } from "../_models/pagination";

@Injectable({ providedIn: 'root' })

export class UserService {
  private basePath = "/api/user/";
  constructor(private http: HttpClient) { }

  getAll(page?: number, itemsPerPage?: number) {
    let params = new HttpParams();

    let paginationResult = new PaginationResult<any[]>();

    if (page && itemsPerPage) {
      params = params
        .append("pageNumber", page)
        .append("pageSize", itemsPerPage);
    }

    return this.http.get<any[]>(this.basePath + "get-all", { observe: 'response', params })
      .pipe(map(response => {
        if (response.body) {
          paginationResult.result = response.body;
        }
        const pagination = response.headers.get('Pagination');

        if (pagination) {
          paginationResult.pagination = JSON.parse(pagination);
        }

        return paginationResult;
      }));
  }

  remove(userId: string) {
    return this.http.delete(this.basePath + "remove?userId=" + userId);
  }
}
