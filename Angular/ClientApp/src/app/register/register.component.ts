import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AccountService } from '../services/account.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  formRegister!: FormGroup<any>;
  confirm: boolean = false; 

  constructor(
    private accountService: AccountService,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private toastr: ToastrService,
    private router: Router) { }

  ngOnInit(): void {

    var id = this.route.snapshot.queryParams['id'];

    this.confirm = !!id;

    if (id) {
      this.accountService.confirm(id).subscribe(
        () => {
          this.toastr.success("Registration confirm");
          this.router.navigate(['/login']);
        },
        error => {
          this.toastr.error(error);
        });

      return;
    }

    this.formRegister = this.formBuilder.group({
      firstName: ['', Validators.required],
      surname: ['', Validators.required],
      patronimic: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      login: ['', Validators.required],
      password: ['', [Validators.required, Validators.minLength(6)]]
    });
  }

  register() {
    if (this.formRegister?.invalid) {
      this.toastr.error("Invalid form");
      return;
    }

    this.accountService.register(this.formRegister?.getRawValue()).subscribe(() => {
      this.toastr.success("Confirm your registration by clicking on the link in the email", "Success", { timeOut: 7000 });
      this.router.navigate(['']);

    }, error => {
      this.toastr.error(error);
    });
  }
}
