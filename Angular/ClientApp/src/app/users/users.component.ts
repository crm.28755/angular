import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { UserService } from '../services/user.service';
import { Pagination } from '../_models/pagination';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  users: any[] = [];
  pagination: Pagination | undefined;
  pageNumber = 1;
  pageSize = 1;
  constructor(private userService: UserService, private toastr: ToastrService) { }

  ngOnInit(): void {
    this.loadUsers();
  }

  loadUsers() {
    this.userService.getAll(this.pageNumber, this.pageSize).subscribe(response => {
      if (response.result && response.pagination) {
        this.users = response.result;
        this.pagination = response.pagination;
      }
    });
  }

  remove(userId: any) {
    this.userService.remove(userId).subscribe(() => {
      this.users = this.users.filter(i => i.id != userId);
      this.toastr.success("Remove successfuly");
    });
  }

  pageChanged(event: any) {
    if (this.pageNumber == event.page) return;
    this.pageNumber = event.page;
    this.loadUsers();
  }
}
