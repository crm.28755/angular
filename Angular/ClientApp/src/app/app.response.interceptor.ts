import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { AccountService } from './services/account.service';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
  constructor(private accountService: AccountService, private router: Router) { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(catchError(err => {

      if (err.status === 401) {
        this.router.navigate(['login']);
        //this.accountService.logout();
        //location.reload();
      }

      const error = this.getErrorMessage(err.error.Error || err.status);

      return throwError(error);
    }));
  }

  private getErrorMessage(error: any) {
    var err = parseInt(error);

    switch (err) {
      case 1: return "Login already exists";
      case 3: return "Invalid login or password";
      default: return error;
    }
  }
}
