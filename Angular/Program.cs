using Angular.BLL.Models;
using Angular.BLL.Services.Interfaces;
using Angular.BLL.Services.Objects;
using Angular.BLL.Validators.Interfaces;
using Angular.BLL.Validators.Objects;
using Angular.DAL;
using Angular.DAL.EF;
using Angular.DAL.Interfaces;
using Angular.Middlewares;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using System.Text;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllersWithViews();

builder.Services.AddDbContext<DbContext, AngularDbContext>(config =>
{
    config.UseSqlServer(builder.Configuration.GetConnectionString("Default"));
});

builder.Services.AddTransient<IRepositoryProvider, RepositoryProvider>();
builder.Services.AddTransient<IUserService, UserService>();
builder.Services.AddTransient<IUserValidator, UserValidator>();
builder.Services.AddTransient<ITokenService, TokenService>();
builder.Services.AddTransient<IMailService, MailService>();

builder.Services.AddHostedService<RegistrationRequestService>();

builder.Services.Configure<MailConfiguration>(options => builder.Configuration.GetSection("MailConfig").Bind(options));

builder.Services
    .AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
    .AddJwtBearer(options =>
    {
        options.TokenValidationParameters = new TokenValidationParameters
        {
            ValidateIssuerSigningKey = true,
            ValidateIssuer = false,
            ValidateAudience = false,
            ValidateLifetime = true,
            IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(builder.Configuration["TokenKey"]))
        };
    });

var app = builder.Build();

if (!app.Environment.IsDevelopment())
{
    app.UseHsts();
}

app.UseHttpsRedirection();
app.UseStaticFiles();
app.UseRouting();

app.UseMiddleware<ExceptionMiddleware>();

app.MapControllers();

app.MapControllerRoute(
    name: "default",
    pattern: "{controller}/{action=Index}/{id?}");

app.MapFallbackToFile("index.html");

app.UseAuthentication();
app.UseAuthorization();

app.Run();


//create separated email service
//create service clear exp request registreation
//create iuservalidator