﻿using Angular.BLL.Models;
using System.Text.Json;

namespace Angular.BLL.DTOs
{
    public class ErrorDetailsDto
    {
        public ErrorDetailsDto(ExceptionServer exception)
        {
            Error = (int)exception.Error;
            Message = exception.Message;
        }

        public int Error { get; }
        public string? Message { get; }
        public override string ToString() => JsonSerializer.Serialize(this);
    }
}