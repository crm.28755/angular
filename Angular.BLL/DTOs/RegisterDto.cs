﻿namespace Angular.BLL.DTOs
{
    public class RegisterDto
    {
        public string FirstName { get; set; } = null!;
        public string Surname { get; set; } = null!;
        public string Patronimic { get; set; } = null!;
        public string Email { get; set; } = null!;
        public string Login { get; set; } = null!;
        public string Password { get; set; } = null!;
    }
}