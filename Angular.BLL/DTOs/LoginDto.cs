﻿namespace Angular.BLL.DTOs
{
    public class LoginDto
    {
        public string Login { get; set; } = null!;
        public string Password { get; set; } = null!;
    }
}