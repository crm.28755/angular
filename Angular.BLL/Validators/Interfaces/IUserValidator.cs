﻿using Angular.BLL.DTOs;

namespace Angular.BLL.Validators.Interfaces
{
    public interface IUserValidator
    {
        Task ValidateRegisterDtoAsync(RegisterDto model);
    }
}