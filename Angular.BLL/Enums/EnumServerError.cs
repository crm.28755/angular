﻿namespace Angular.BLL.Enums
{
    public enum EnumErrorServer
    {
        Unknown = 0,
        LoginAlreadyExists = 1,
        NotFoundActualRegistrationRequest = 2,
        InvalidUserNameOrPassword = 3,
        PasswordEmpty = 4,
        LoginEmpty = 5,
        EmailEmpty = 6,
        FirstNameEmpty = 7,
        SurnameEmpty = 8,
        PatronymicEmpty = 9,
        InvalidPasswordLength = 10,
        InvalidLoginLength = 11,
        InvalidEmailLength = 12
    }
}