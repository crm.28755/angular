﻿using Angular.BLL.Enums;
using Angular.BLL.Models;

namespace Angular.BLL.Extensions
{
    public static class ExceptionServerExtensions
    {
        public static ExceptionServer GetExceptionServer(this Exception exception)
        {
            if (exception is ExceptionServer res) return res;

            return new ExceptionServer(EnumErrorServer.Unknown, exception.Message);
        }
    }
}