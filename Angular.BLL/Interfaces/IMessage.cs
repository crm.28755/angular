﻿namespace Angular.BLL.Interfaces
{
    public interface IMessage
    {
        string Email { get; }
        string Subject { get; }
        string Body { get; }
        bool IsHtml { get; }
    }
}