﻿using Angular.BLL.Interfaces;

namespace Angular.BLL.Models
{
    public class Message : IMessage
    {
        public string Email { get; set; } = null!;
        public string Subject { get; set; } = null!;
        public string Body { get; set; } = null!;
        public bool IsHtml { get; set; }
    }
}