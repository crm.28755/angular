﻿namespace Angular.BLL.Models
{
    public class MailConfiguration
    {
        public string Email { get; set; } = null!;
        public string Password { get; set; } = null!;
    }
}