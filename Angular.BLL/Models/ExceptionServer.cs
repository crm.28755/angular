﻿using Angular.BLL.Enums;

namespace Angular.BLL.Models
{
    public class ExceptionServer : Exception
    {
        public EnumErrorServer Error { get; }
        public ExceptionServer(EnumErrorServer error) : base()
        {
            Error = error;
        }
        public ExceptionServer(EnumErrorServer error, string message) : base(message)
        {
            Error = error;
        }
    }
}