﻿using Angular.BLL.DTOs;
using Angular.BLL.Enums;
using Angular.BLL.Models;
using Angular.BLL.Services.Interfaces;
using Angular.BLL.Validators.Interfaces;
using Angular.DAL.Entities;
using Angular.DAL.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System.Security.Cryptography;
using System.Text;

namespace Angular.BLL.Services.Objects
{
    public class UserService : IUserService
    {
        private readonly IRepositoryProvider _repProvider;
        private readonly IUserValidator _validator;
        private readonly ILogger<UserService> _logger;
        private readonly IMailService _mailService;

        public UserService(
            IRepositoryProvider repProvider, 
            IUserValidator validator, 
            IMailService mailService,
            ILogger<UserService> logger) 
        {
            _repProvider = repProvider;
            _validator = validator;
            _mailService = mailService;
            _logger = logger;
        }

        public async Task RegisterAsync(RegisterDto model)
        {
            try
            {
                await _validator.ValidateRegisterDtoAsync(model);

                var login = model.Login.Trim();

                var existsLogin = await _repProvider.Users
                    .Select()
                    .AnyAsync(i => i.Login == login); //заменить на кеш? перевести на валидатор //redis
                                                      //создать сервис которій будет в память загружать данніе для проверки логинов
                                                      // сбрасівать кеш при регистрации

                if (existsLogin) throw new ExceptionServer(EnumErrorServer.LoginAlreadyExists);

                var utcNow = DateTime.UtcNow;

                using var hmac = new HMACSHA512();

                var registrationRequest = new RegistrationRequest
                {
                    Id = Guid.NewGuid(),
                    DateCreated = utcNow,
                    DateExpiration = utcNow.AddDays(1),
                    FirstName = model.FirstName,
                    Surname = model.Surname,
                    Login = login,
                    Email = model.Email,
                    Patronymic = model.Patronimic,
                    PasswordHash = hmac.ComputeHash(Encoding.UTF8.GetBytes(model.Password)),
                    PasswordSalt = hmac.Key
                };

                await _repProvider.RegistrationRequets.AddAsync(registrationRequest);
                await _repProvider.SaveChangesAsync();

                await SendMessageToUserEmailForConfirmationResistrationAsync(registrationRequest);
            }
            catch (Exception e)
            {
                _logger.LogError($"Error registration: {e.Message}");
                throw;
            }
        }

        public async Task ConfirmAsync(Guid id)
        {
            try
            {
                var registrationRequest = await _repProvider.RegistrationRequets.FindActualAsync(id);

                if (registrationRequest == null) throw new ExceptionServer(EnumErrorServer.NotFoundActualRegistrationRequest);

                var user = new User
                {
                    Id = Guid.Empty,
                    DateCreated = DateTime.UtcNow,
                    DateUpdate = DateTime.UtcNow,
                    Login = registrationRequest.Login,
                    Email = registrationRequest.Email,
                    FirstName = registrationRequest.FirstName,
                    Patronymic = registrationRequest.Patronymic,
                    Surname = registrationRequest.Surname,
                    PasswordSalt = registrationRequest.PasswordSalt,
                    PasswordHash = registrationRequest.PasswordHash,
                };

                using (var trans = await _repProvider.BeginTransactionAsync())
                {
                    try
                    {
                        await _repProvider.Users.AddAsync(user);
                        _repProvider.RegistrationRequets.Remove(registrationRequest);
                        await _repProvider.SaveChangesAsync();
                        await trans.CommitAsync();
                    }
                    catch (Exception)
                    {
                        await trans.RollbackAsync();
                        throw;
                    }
                }

                await SendMessgaeToUserSuccessRegistrationAsync(user);
            }
            catch (Exception e)
            {
                _logger.LogError($"Error confirm registration: {e.Message}");
                throw;
            }
        }

        private async Task SendMessageToUserEmailForConfirmationResistrationAsync(RegistrationRequest registrationRequest)
        {
            var message = new Message
            {
                Body = $"Go to <a href='https://localhost:44406/register?id={registrationRequest.Id}'>link</a> for to finished resgistration!",
                Email = registrationRequest.Email,
                IsHtml = true,
                Subject = "Confirm registration"
            };

            await _mailService.SendAsync(message);
        }
        private async Task SendMessgaeToUserSuccessRegistrationAsync(User user)
        {
            var message = new Message
            {
                Body = "Success registration user " + user.Login,
                Email = user.Email,
                IsHtml = false,
                Subject = "Success registration"
            };

            await _mailService.SendAsync(message);
        }

        public async Task<User> LoginAsync(LoginDto model)
        {
            var user = await _repProvider.Users
                .Select()
                .SingleOrDefaultAsync(i => i.Login == model.Login);

            if (user == null) throw new ExceptionServer(EnumErrorServer.InvalidUserNameOrPassword);

            using var hmac = new HMACSHA512(user.PasswordSalt);

            var computedHash = hmac.ComputeHash(Encoding.UTF8.GetBytes(model.Password));

            for (var i = 0; i < computedHash.Length; i++)
            {
                if (computedHash[i] != user.PasswordHash[i])
                {
                    throw new ExceptionServer(EnumErrorServer.InvalidUserNameOrPassword);
                }
            }

            return user;
        }
    }
}