﻿using Angular.BLL.Interfaces;
using Angular.BLL.Models;
using Angular.BLL.Services.Interfaces;
using Microsoft.Extensions.Options;
using System.Net;
using System.Net.Mail;

namespace Angular.BLL.Services.Objects
{
    public class MailService : IMailService
    {
        private readonly NetworkCredential _credential;
        private readonly MailAddress _from;
        public MailService(IOptions<MailConfiguration> optionConfiguration)
        {
            var configuration = optionConfiguration.Value;

            _credential = new NetworkCredential(configuration.Email, configuration.Password);
            _from = new MailAddress(configuration.Email);
        }
        public async Task SendAsync(IMessage message)
        {
            var to = new MailAddress(message.Email);
            var mailMessage = new MailMessage(_from, to);

            mailMessage.Subject = message.Subject;
            mailMessage.Body = message.Body;
            mailMessage.IsBodyHtml = message.IsHtml;

            var smtp = new SmtpClient("smtp.gmail.com", 587);

            smtp.UseDefaultCredentials = false;

            smtp.Credentials = _credential;
            smtp.EnableSsl = true;

            await smtp.SendMailAsync(mailMessage);
        }
    }
}