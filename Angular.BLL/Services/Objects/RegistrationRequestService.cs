﻿using Angular.DAL.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace Angular.BLL.Services.Objects
{
    public class RegistrationRequestService : BackgroundService
    {
        private readonly IServiceProvider _serviceProvider;
        private readonly ILogger<RegistrationRequestService> _logger;
        private readonly TimeSpan _frequencyCheck = TimeSpan.FromSeconds(60);

        public RegistrationRequestService(
            IServiceProvider serviceProvider,
            ILogger<RegistrationRequestService> logger
            )
        {
            _serviceProvider = serviceProvider;
            _logger = logger;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            await HandleAsync(stoppingToken);
        }

        private async Task HandleAsync(CancellationToken? cancellationToken)
        {
            while (true)
            {
                using (var scope = _serviceProvider.CreateScope())
                {
                    var repProvider = scope.ServiceProvider.GetRequiredService<IRepositoryProvider>();

                    try
                    {
                        await repProvider.RegistrationRequets.SelectExpired().ExecuteDeleteAsync();
                    }
                    catch when (cancellationToken!.Value.IsCancellationRequested)
                    {
                        return;
                    }
                    catch (Exception e)
                    {
                        _logger.LogError(e.Message);
                    }
                    finally
                    {
                        await Task.Delay(_frequencyCheck);
                    }
                }
            }
        }
    }
}