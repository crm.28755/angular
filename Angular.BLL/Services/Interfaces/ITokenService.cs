﻿using Angular.DAL.Entities;

namespace Angular.BLL.Services.Interfaces
{
    public interface ITokenService
    {
        string CreateToken(User user);
    }
}