﻿using Angular.BLL.DTOs;
using Angular.DAL.Entities;

namespace Angular.BLL.Services.Interfaces
{
    public interface IUserService
    {
        Task RegisterAsync(RegisterDto model);
        Task ConfirmAsync(Guid id);
        Task<User> LoginAsync(LoginDto model);
    }
}