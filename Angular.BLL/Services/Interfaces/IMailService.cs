﻿using Angular.BLL.Interfaces;

namespace Angular.BLL.Services.Interfaces
{
    public interface IMailService
    {
        Task SendAsync(IMessage message);
    }
}